<?php

namespace App\Hg;

/**
 * Class PartnerLoginConfig
 * @package App\Hg
 */
class PartnerLoginConfig
{
    protected $countryId;
    protected $url;

    public function __construct()
    {
        global $_APP;

        $this->countryId = ...;
        $this->url = '...';
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId(int $countryId): void
    {
        $this->countryId = $countryId;
    }
}