<?php

namespace App\Hg;

use GuzzleHttp\Client;

/**
 * Class PartnerLogin
 * @package App\Hg
 */
class PartnerLogin extends PartnerLoginConfig
{
    protected $method  = "...";
    protected $user = "...";
    protected $pass = "...";
    protected $response;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getResponse()
    {
        $header = [
            "Content-Type" => "application/json"
        ];

        $body = [
            "login" => $this->user,
            "password" => $this->pass,
            "goal" => "web",
            "countryId" => $this->getCountryId()
        ];

        $this->api($header, $body);

        return $this->response;
    }

    /**
     * @param array $header
     * @param array $body
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function api(array $header, array $body)
    {
        try {
            $client = new Client();
            $response = $client->request(
                "POST",
                $this->getUrl() . $this->method,
                [
                    'headers' => $header,
                    'body' => json_encode($body)
                ]
            );

            $response = json_decode($response->getBody()->getContents())->payload;
        } catch (GuzzleHttp\Exception $exc) {
            $response = $exc->getResponse()->getBody();
        } catch (\Exception $exc) {
            $response = null;
        }

        $this->response = $response;
    }
}
