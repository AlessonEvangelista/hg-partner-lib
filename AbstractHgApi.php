<?php

namespace App\Hg;

use App\Tool\Helper;

/**
 * Class AbstractHgApi
 * @package App\Hg
 */
abstract class AbstractHgApi extends PartnerLogin
{
    protected static $partnerLogin;

    function __construct()
    {
        parent::__construct();
        self::getPartnerToken();
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected static function getPartnerToken()
    {
        if (self::$partnerLogin) {
            return self::$partnerLogin;
        }
        global $_APP;

        $partnerLogin = Helper::getCache()->getItem('HgTokenValidation')->get();

        if ( is_null($partnerLogin) || !isset($partnerLogin->shelf_life) ) {

            $token = new PartnerLogin();
            $partnerLogin = $token->getResponse();

            // Tempo do Token no Cache: 8 horas
            Helper::setCache('HgTokenValidation', $partnerLogin, 43200 );
        }

        self::$partnerLogin = $partnerLogin;
        return self::$partnerLogin;
    }

}
