<?php
declare(strict_types=1);

namespace App\Hg;

/**
 * Class HgApi
 * @package App\Hg
 */
class HgApi extends AbstractHgApi
{

    /**
     * @return mixed
     */
    public function getTokenLogin(){
        return self::$partnerLogin;
    }

}